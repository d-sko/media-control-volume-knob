# media-control-volume-knob

This repository contains the arduino source for the media control volume knob by PrusaPrinters which can be found [here](https://blog.prusaprinters.org/3d-print-an-oversized-media-control-volume-knob-arduino-basics/), with some modifications by me.

# changes
- encoder clicks synchronized to volume changes (#2,#5)
- RX/TX LEDs disabled (#3)
- muting with long press (#4)